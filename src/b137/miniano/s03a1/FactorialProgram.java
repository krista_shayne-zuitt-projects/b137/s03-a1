package b137.miniano.s03a1;

import java.util.Scanner;

public class FactorialProgram {
    public static void main(String[] args) {
        System.out.println("Factorial Program\n");

        int num, i = 1;
        long factorial = 1;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter any number:");
        num = scan.nextInt();
        scan.close();

        while(i <= num)
        {
            factorial *= i;
            i++;
        }
        System.out.printf("Factorial of %d = %d", num, factorial);
    }

}
